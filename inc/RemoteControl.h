#pragma once

#include "AutomationGlobal.h"
#include <QNetworkProxy>
#include <QObject>
#include <QTcpServer>
#include <AnalyzerItem.h>


namespace Planar {
namespace Automation {

const int DefaultSocketPort = 5025;
const int DefaultHislipPort = 4880;

class AUTOMATION_EXPORT RemoteControl : public AnalyzerItem
{
    Q_OBJECT
    //ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(BoolProperty* enabled READ enabled NOTIFY automationPropertyAdded)
    Q_PROPERTY(IntProperty* port READ port NOTIFY automationPropertyAdded)

public:
    RemoteControl(AnalyzerItem* parent);
    ~RemoteControl() override;
    void close();
    bool open(qint16 const port,
              QNetworkProxy const& proxy = QNetworkProxy::NoProxy,
              QHostAddress const& address = QHostAddress(QHostAddress::Any));
    qint16 port() const;
    //void EmitResetCommand() {emit ResetCommand(Analyzer::ScannerMode::Measurement);}
    BoolProperty* enabled();
    IntProperty* port();

public slots:
    //void acceptError(QAbstractSocket::SocketError);
    virtual void acceptConnection() = 0;
    virtual void onEnabledChanged(bool value);
    virtual void onPortChanged(int port);

signals:
    void error (QString msg);
    void listen ();
    //void maketestVerification();
    void stateDescription(QString msg);
    //void ResetCommand(Analyzer::ScannerMode mode);
    //void acceptConnectionSignal();
    void closeSockets();
    void automationPropertyAdded();

protected:
    QTcpServer _tcpServer;
    BoolProperty _enabledProperty;
    IntProperty _portProperty;
    int _port;

private slots:
    //virtual void OnScanCompleted(Analyzer::ScannerMode mode) = 0;     //TODO вернуть метод
    void onDeviceDisconnect();
};

class AUTOMATION_EXPORT SocketRemoteControl : public RemoteControl
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    SocketRemoteControl(AnalyzerItem* parent,
                        qint16 const port = DefaultSocketPort);
    ~SocketRemoteControl() override;

public slots:
    virtual void acceptConnection();
};

}//Automation
}//Planar

#pragma once

#include <QByteArray>
#include "AutomationGlobal.h"
#include "cmds.h"
#include "scpi.h"


namespace Planar {
namespace Automation {

class DeviceFunctions;

typedef void (*Handler)(DeviceFunctions * const,
                        strParam const * const,
                        unsigned int const * const);


struct AUTOMATION_EXPORT Command {
    Command(QByteArray && commandText, Handler handler, strSpecParam const& param = {NOP});
    static bool _sweepFlag;
    //static bool _opcFlag;
    //static bool _waiFlag;
    QByteArray commandText;
    Handler handler;
    strSpecParam specParam;
};


}//Automation
}//Planar

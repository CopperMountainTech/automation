#ifndef AUTOMATIONITEM_H
#define AUTOMATIONITEM_H

#include <QObject>
#include <AnalyzerItem.h>

#include "AutomationGlobal.h"
#include "RemoteControl.h"
#include "HislipRemoteControl.h"

namespace Planar {
namespace Automation {

class AUTOMATION_EXPORT AutomationItem : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(SocketRemoteControl* socketControl READ socketControl
               NOTIFY automationPropertyAdded)
    Q_PROPERTY(HislipRemoteControl* hislipControl READ hislipControl
               NOTIFY automationPropertyAdded)

public:
    AutomationItem(AnalyzerItem* parent);
    ~AutomationItem() override;
    SocketRemoteControl* socketControl();
    HislipRemoteControl* hislipControl();

signals:
    void automationPropertyAdded();

private:
    SocketRemoteControl _socketControl;
    HislipRemoteControl _hislipControl;
};

}//Automation
}//Planar

#endif // AUTOMATIONITEM_H

#pragma once

#include <stdexcept>
#include <QHash>
#include <cmds.h>
#include <scpi.h>

namespace Planar {
namespace Automation {

class AUTOMATION_EXPORT AutomationException : public std::exception
{
public:
    explicit AutomationException(int code) : exception(/*"Automation exception", code*/)
    {_code = code;}
private:
    int _code;
public:
    int Code() {return _code;}
};

// Define command error codes (100..199)
// Codes 1-78 defined in scpi.h for scpi parser errors
#define CMD_ERR_UNKNOWN								(100)
#define CMD_ERR_TRIGGER_RECEIVED			(105)
#define CMD_ERR_SUFFIX_OUT_OF_RANGE		(114)
#define CMD_ERR_INPUT_BUFFER_IS_FULL	(115)

// Define device specific error codes (300..399)
#define DEV_ERR_UNKNOWN						 (300)
#define DEV_ERR_CREATE_OBJECT			 (310)
#define DEV_ERR_STATUS_REPORTING	 (302)

// Define query error codes (400..499)
#define QUERY_ERR_UNKNOWN					 (400)
#define QUERY_ERR_MSG_INTERRUPTED  (410)
#define QUERY_ERR_MSG_UNTERMINATED (420)
#define QUERY_ERR_MSG_NO_DATA			 (403)
#define QUERY_ERR_MSG_TRUNC				 (404)

// Define execution error codes (200..499)
#define EXEC_ERR_UNKNOWN						(200)

#define EXEC_ERR_CHAN_INDEX				(201)
#define EXEC_ERR_TRACE_INDEX			(202)
#define EXEC_ERR_MARKER_INDEX			(203)
#define EXEC_ERR_MARKER_NOT_ACTIVE      (204)
#define EXEC_ERR_SAVE_TYPE_SPEC			(205)
#define EXEC_ERR_SWEEP_TYPE_SPEC		(206)
#define EXEC_ERR_TRIG_SOURCE_SPEC		(207)
#define EXEC_ERR_S_PARAM_SPEC			(208)
#define EXEC_ERR_FORMAT_SPEC			(209)
#define EXEC_ERR_DATA_MATH_SPEC			(210)
#define EXEC_ERR_TRIG_NOT_WAIT			(211)
#define EXEC_ERR_TRIG_MODE  			(212)
#define EXEC_ERR_INIT_IGNORED 			(213)
#define EXEC_ERR_LIMIT_DATA				(214)
#define EXEC_ERR_SEGMENT_DATA			(215)
#define EXEC_ERR_STD_SPEC   			(216)
#define EXEC_ERR_CONV_SPEC   			(217)
#define EXEC_ERR_GAT_SHAPE_SPEC			(218)
#define EXEC_ERR_GAT_TYPE_SPEC			(219)
#define EXEC_ERR_PARAM_ERROR			(220)
#define EXEC_ERR_PORT_INDEX				(221)
#define EXEC_ERR_DATA_OUT_RANGE			(222)
#define EXEC_ERR_ILLEGAL_PARAM			(224)
#define EXEC_ERR_FILE_NOT_FOUND         (256)


AUTOMATION_EXPORT extern QHash<int, QString> ErrorsDictionary;

AUTOMATION_EXPORT void FillErrorsDictionary();


}//Automation
}//Planar

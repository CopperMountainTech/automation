#ifndef HISLIPREMOTECONTROL_H
#define HISLIPREMOTECONTROL_H

#include <QObject>
#include "AutomationGlobal.h"
#include "RemoteControl.h"
#include "HislipRemoteClient.h"

namespace Planar {
namespace Automation {

class AUTOMATION_EXPORT HislipRemoteControl : public RemoteControl
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
public:
    HislipRemoteControl(AnalyzerItem* parent, quint16 const port = DefaultHislipPort);
    void close();
    ~HislipRemoteControl() override;


public slots:
    virtual void acceptConnection();


private slots:
    void onClientAccepted(HislipRemoteClient* client);

private:
    quint32 _clientCounter;

    //клиенты, у которых не инициализировано асинхронное подключение
    QMap<quint32, HislipRemoteClient*> _clients;
};

class HislipConnectionHelper : public QObject
{
    Q_OBJECT
public:
    HislipConnectionHelper(QTcpSocket* const connection, quint32 sessionId,
                           QMap<quint32, HislipRemoteClient*>* clientMap,
                           QObject* parent = nullptr);

private:
    QTcpSocket* const _connection;
    quint32 _currentSessionId;
    QMap<quint32, HislipRemoteClient*>* _clientMap;

signals:
    void completed();
    void clientAccepted(HislipRemoteClient* client);

private slots:
    void onNewMessage();
};

}//Automation
}//Planar

#endif // HISLIPREMOTECONTROL_H

#pragma once

#include <QByteArray>
#include <array>
#include "Command.h"
#include "RemoteClient.h"


namespace Planar {
namespace Automation {


const size_t maxCmdSize = 1000;

using Handlers = std::array<void (*)(DeviceFunctions * const,
                                     strParam const * const,
                                     unsigned int const * const), maxCmdSize>;


class AUTOMATION_EXPORT DeviceFunctions {
public:
    DeviceFunctions(RemoteClient * remoteClient, QByteArray& answer);
    DeviceFunctions (DeviceFunctions const &) = delete;
    DeviceFunctions (DeviceFunctions &&) = delete;
    DeviceFunctions& operator = (DeviceFunctions const &) = delete;
    DeviceFunctions& operator = (DeviceFunctions &&) = delete;
    ~DeviceFunctions() = default;

public:
    static void registerCommand(Command& command);
    void ParseMessage(QByteArray &buffer);
    //void SystLocCommand();
    //void SystRemCommand();
    //void SystRwlCommand();
    //void SystHideCommand();
    //void SystShowCommand();

    //int CheckStandart(int Std);

    //void ExecutionError();
    //void QueueError(int const code);

    template <size_t index> void handler();
    //template <typename ...T> void sendAnswer (T const & ...arg);
    void sendAnswer(const QByteArray& message);

    int GetLastError() {return _remoteClient->GetLastError();}

    void SetWai(bool value) {_remoteClient->SetWai(value); }
    void SetOpc(bool value) {_remoteClient->SetOpc(value); }
    bool GetWai() {return _remoteClient->GetWai(); }
    bool GetOpc() {return _remoteClient->GetOpc(); }

private:
    static size_t _commandCount;
    int _error;
    //int _currentPortIndex;
    //int m_Std;
    QByteArray& _answer;
    bool _executeStatus;
    RemoteClient * const _remoteClient;                 //для доступа к очереди ошибок
};


}//Automation
}//Planar

#pragma once

#include <QObject>
#include <QTcpSocket>
#include <QQueue>
#include <QTimer>

#include "RemoteControl.h"


namespace Planar {
namespace Automation {

class AUTOMATION_EXPORT RemoteClient : public QObject
{
    Q_OBJECT
public:
    RemoteClient(QTcpSocket* connection, QObject* parent = nullptr);
    ~RemoteClient();
    RemoteClient (RemoteClient const&) = delete;
    RemoteClient (RemoteClient&&) = delete;
    RemoteClient& operator = (RemoteClient const&) = delete;
    RemoteClient& operator = (RemoteClient&&) = delete;
    //~RemoteClient () = default;
    virtual void onReadyRead () = 0;
    int GetLastError();
    void SetOpc(bool value)
    {
        _opcFlag = value;
    }
    void SetWai(bool value)
    {
        _waiFlag = value;
    }
    bool GetWai()
    {
        return _waiFlag;
    }
    bool GetOpc()
    {
        return _opcFlag;
    }

protected:
    void tryProcessMessage(QByteArray message, QByteArray& answer);

    QTcpSocket* const _connection;
    QQueue<int> _errorQueue;
    QTimer _timer;
    QByteArray _answer;
    bool _opcFlag;
    bool _waiFlag;


private slots:
    //void OnScanCompleted(ScannerMode mode);     //TODO вернуть метод
    void onDeviceDisconnect();
    virtual void parseMessage() = 0;                            //to SocketRemoteClient?
};

class AUTOMATION_EXPORT SocketRemoteClient : public RemoteClient
{
    Q_OBJECT

public:
    SocketRemoteClient(QTcpSocket* connection, QObject* parent = nullptr);
    ~SocketRemoteClient();
    virtual void onReadyRead();

private slots:
    virtual void parseMessage();

private:
    QQueue<QString> _commands;
};

}//Automation
}//Planar

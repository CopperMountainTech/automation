#pragma once

#include <functional>
#include <vector>

#include <QByteArray>

#include <cmds.h>
#include <scpi.h>
//#include <Complex.h>

#include "AutomationException.h"

struct strParam;

namespace Planar {
namespace Automation {

class AUTOMATION_EXPORT ParametersSequence {
public:
    void addParameter(const QString& parameterName);
    const char* parameters();

private:
    QByteArray _parametersContainer;
};

AUTOMATION_EXPORT double NumericMinMaxPow2dBm(strParam const * const arg);

AUTOMATION_EXPORT bool toBool (strParam const * const arg);

AUTOMATION_EXPORT double toDouble (strParam const * const arg);

AUTOMATION_EXPORT std::vector<double> toDoubleVector( strParam const * const arg);

AUTOMATION_EXPORT unsigned long toEnumConstant (strParam const * const arg);

AUTOMATION_EXPORT QString toCharData(strParam const * const arg);

AUTOMATION_EXPORT long toLong(strParam const * const arg);

AUTOMATION_EXPORT enParamType toParamType (strParam const * const arg);

AUTOMATION_EXPORT QString toString (strParam const * const arg);

AUTOMATION_EXPORT enUnits toUnits (strParam const * const arg);

AUTOMATION_EXPORT unsigned long toUnsignedInt(strParam const * const arg);

AUTOMATION_EXPORT unsigned long toUnsignedLong(strParam const * const arg);

//AUTOMATION_EXPORT QByteArray toQByteArray(Planar::VNA::TriggerSourceType arg);

//AUTOMATION_EXPORT QByteArray toByteArray(Analyzer::SweepType arg);

//AUTOMATION_EXPORT QByteArray toQByteArray(Analyzer::MeasurementType arg);

//AUTOMATION_EXPORT QByteArray toQByteArray(Planar::Dsp::Complex<double> const & arg);

AUTOMATION_EXPORT QByteArray toQByteArray(bool arg);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::DataMathType code);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::TraceFormatType code);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::PeakPolarityType code);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::MarkerSearchType code);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::TargetTransType code);

//AUTOMATION_EXPORT QByteArray toQByteArray(std::function<Planar::Dsp::Complex<double>(int const)> const & fun,
//                        int const size);

//AUTOMATION_EXPORT QByteArray toQByteArray(std::function<Planar::Dsp::Complex<double>(size_t const)> const & fun,
//                        size_t const size);

//AUTOMATION_EXPORT QByteArray toQByteArray (Planar::VNA::ReferenceSourceType code);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::ResponseType code);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::ConversionType code);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::GatingType arg);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::SearchType arg);

//AUTOMATION_EXPORT QByteArray toQByteArray (Analyzer::SweepType arg);

AUTOMATION_EXPORT QByteArray toQByteArray (size_t arg);

template <typename T> QByteArray toQByteArray(T arg)          //TODO return
{
    return QByteArray::number(arg) + '\n';
}

template <typename ...T2> QByteArray toQByteArray (size_t arg, T2 ...args) {
    return QByteArray::number(static_cast<int>(arg)) + ',' + toQByteArray (args...);
}

template <typename T1, typename ...T2> QByteArray toQByteArray (T1 arg, T2 ...args) {
    return QByteArray::number(arg) + ',' + toQByteArray (args...);
}

AUTOMATION_EXPORT QString shortScpiForm(QString input);



}//Automation
}//Planar

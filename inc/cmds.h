/**************************************************************************************/
/* JPA-SCPI PARSER SOURCE CODE MODULE																									*/
/* (C) JPA Consulting Ltd., 2004	(www.jpacsoft.com)																	*/
/*																																										*/
/* View this file with tab spacings set to 2																					*/
/*																																										*/
/* cmds.h																																							*/
/* ======																																							*/
/*																																										*/
/* Module Description																																	*/
/* ------------------																																	*/
/* Contains definitions specific to your compiler.																		*/
/* Contains information specific to the SCPI command set supported by your instrument.*/
/*																																										*/
/* Where indicated "USER", you will be instructed to modify the lines of code to			*/
/* support your instrument's requirements.																						*/
/*																																										*/
/* Full instructions regarding how to modify this file to suit your requirements is		*/
/* given in the JPA-SCPI PARSER USER MANUAL - Do not attempt to make modifications		*/
/* until you have read the documentation.																							*/
/*																																										*/
/* Module Revision History																														*/
/* -----------------------																														*/
/* V1.0.0:15/04/02:Initial Release																										*/
/* V1.1.0:29/08/02:Modified for compatibility with JPA SCPI Parser V1.2.0							*/
/* V1.2.0:08/07/04:Modify for JPA SCPI Parser V1.3.0: Add variable type definitions		*/
/**************************************************************************************/

/* Only include this header file once */
#ifndef CMDS_H
#define CMDS_H

#ifdef __cplusplus
extern "C" {
#endif


/**************************************************************************************/
/* Optional Support Features																													*/
/* -------------------------																													*/
/* USER: #define the features that you require and comment out those not required			*/
#define SUPPORT_NUM_SUFFIX				/* Numeric Suffix in keywords		 										*/
#define SUPPORT_NUM_LIST					/* Numeric List parameter type											*/
//#define SUPPORT_CHAN_LIST				/* Channel List parameter type 											*/
//#define SUPPORT_EXPR						/* Expression parameter type												*/
/**************************************************************************************/


/**************************************************************************************/
/* Variable Types																																			*/
/* --------------																																			*/
/* USER: If you require, modify the #defines below in order to change the types of		*/
/*			 variables used in the library.																								*/
//#define SCPI_CHAR_IDX		unsigned char		/* Index to char in Input Command line			*/
#define SCPI_CHAR_IDX		unsigned int			/* Index to char in Input Command line			*/
#define SCPI_CMD_NUM		unsigned int			/* Command number														*/
/**************************************************************************************/


/**************************************************************************************/
/* Maximum Numeric Values supported by your Compiler																	*/
/* -------------------------------------------------																	*/
/* USER: Replace these values with the limits of the compiler you are using.					*/
/*			 Alternatively, if you wish you can replace these definitions with						*/
/*			 "#include <limits.h>", if your compiler provides that file.									*/
//#define ULONG_MAX		(0xFFFFFFFF)/* Max possible val of an unsigned long integer			*/
//#define LONG_MAX		(0x7FFFFFFF)/* Max possible val of a signed long integer				*/
//#define UINT_MAX		(0xFFFF)		/* Max possible val of an unsigned integer					*/
//#define INT_MAX			(0x7FFF)		/* Max possible val of a signed integer							*/
//#define	UCHAR_MAX		(0xFF)			/* Max possible val of an unsigned character 				*/
#include <limits.h>
/**************************************************************************************/


/**************************************************************************************/
/* Base Unit Types																																		*/
/* ---------------																																		*/
/* USER: Add Base Unit Types supported by your instrument															*/
/*       Optional: Remove Base Unit Types not supported																*/
/**************************************************************************************/
enum enUnits
{
	U_NONE,								/* USER: Do not modify this line															*/

	U_VOLT,								/* User-modifiable list of supported base unit types					*/
	U_AMP,
	U_OHM,
	U_WATT,
	U_DB_W,
	U_DBM,
	U_JOULE,
	U_FARAD,
	U_HENRY,
	U_HERTZ,
	U_SEC,
	U_KELVIN,
	U_CELSIUS,
	U_FAHREN,
	U_DB,
	U_UNIT,
	U_DEG,

	U_END									/* USER: Do not modify this line															*/
};

/**************************************************************************************/
/* Maximum Parameters																																	*/
/* ------------------																																	*/
/* USER: Modify this value to be equal to the maximum number of parameter accepted		*/
/*			 by any of the supported Command Specs																				*/
#define MAX_PARAMS								(4)				/* Most params accepted by any command		*/
/**************************************************************************************/


#ifdef SUPPORT_NUM_SUFFIX
/**************************************************************************************/
/* Numeric Suffix																																			*/
/* --------------																																			*/
/* (only used if Numeric Suffix support feature is enabled)														*/
/*																																										*/
/* USER: Modify these values as required. See User Manual for more information.				*/
#define MAX_NUM_SUFFIX				(3)						/* Maximum number of numeric suffices			*/
																						/* possible in a single command						*/
#define NUM_SUF_MIN_VAL				(1)						/* Minimum value allowed (0 or greater)		*/
#define NUM_SUF_MAX_VAL				(16)					/* Maximum value allowed (<=UINT_MAX)			*/
#define NUM_SUF_DEFAULT_VAL		(1)						/* Default value if no suffix present			*/
/**************************************************************************************/
#endif


#ifdef SUPPORT_CHAN_LIST
/**************************************************************************************/
/* Maximum Dimensions allowed in a Channel List Entry 																*/
/* --------------------------------------------------																	*/
/* (only used if Channel List support feature is enabled)															*/
/*																																										*/
/* USER: Modify this value to be equal to the maximum number of dimensions that are		*/
/*       allowed in any of the channel list parameters.																*/
/* See User Manual for more information.																							*/
#define MAX_DIMS						(3)							/* Maximum dimensions in a channel list		*/
/**************************************************************************************/
#endif


/**************************************************************************************/
/* Definitions for use in the Command Spec Table    */
/*      */
/* USER: DO NOT MODIFY THESE LINES OF CODE   */
/*      */
/* Optional / Required / No Parameter    */
#define OPT 1,  /* Optional parameter  */
#define REQ 0,  /* Required parameter  */
#define NOP REQ  P_NONE, (void *)0 /* No paramater  */
///*      */
///* Parameter Types     */
#define CH_DAT P_CH_DAT, (void *)& /* Character Data  */
#define BOOLEAN P_BOOL, (void *)& /* Boolean   */
#define NUM P_NUM, (void *)& /* Numerical Value  */
#define STRING P_STR, (void *)0 /* String (quoted)  */
#define UNQ_STR P_UNQ_STR, (void *)0 /* Unquoted String  */
#ifdef SUPPORT_EXPR
#define EXPR P_EXPR, (void *)0 /* Expression  */
#endif
#ifdef SUPPORT_NUM_LIST
#define NUM_L P_NUM_LIST, (void *)& /* Numeric List  */
#endif
#ifdef SUPPORT_CHAN_LIST
#define CH_L P_CHAN_LIST, (void *)& /* Channel List  */
#endif
/**************************************************************************************/


/**************************************************************************************/
/* More definitions for use in the Command Spec Table   */
/*      */
/* USER: Modify as instructed    */
/*      */
/* Command Without Parameters    */
/* USER: Modify to match your value of MAX_PARAMS, e.g. for 3, use {NOP},{NOP},{NOP} */
#define NO_PARAMS {NOP},{NOP},{NOP},{NOP}
/*      */
/* USER: DO NOT MODIFY THIS LINE:    */
#define END_OF_COMMAND_SPECS {{ NO_PARAMS }}
/**************************************************************************************/


/**************************************************************************************/
/* Miscellaneous Definitions & Declarations used in this Module  */
/* ------------------------------------------------------------  */
/* USER: DO NOT MODIFY THESE LINES OF CODE   */
/*      */
/* Boolean Param Spec Declarations    */
extern const struct strSpecAttrBoolean sBNoDef;/* Boolean (no default value) */
extern const struct strSpecAttrBoolean sBDefOn; /* Boolean (default=1 [ON]) */
extern const struct strSpecAttrBoolean sBDefOff; /* Boolean (default=0 [OFF]) */
/*      */
/* Character Data Param Spec Definitions    */
#define CHDAT_SEQ const char  /* Char Data Sequence */
#define CHDAT_TYPE const struct strSpecAttrCharData /* Char Data Attribs */
#define NO_DEF (255) /* No default item number */
#define ALT_NONE P_NONE, (void *)(0) /* No alternative type of param */
/*      */
#ifdef SUPPORT_NUM_LIST
/* Numeric List Param Spec Definitions   */
#define NUMLIST_TYPE const struct strSpecAttrNumList /* Numeric List Attribs */
#endif
#ifdef SUPPORT_CHAN_LIST
/* Channel List Param Spec Definitions   */
#define CHANLIST_TYPE const struct strSpecAttrChanList /* Channel List Attribs */
#endif
/**************************************************************************************/


#include "scpi.h"
extern const struct strSpecUnits sSpecUnits[];


#ifdef __cplusplus
}
#endif

#endif

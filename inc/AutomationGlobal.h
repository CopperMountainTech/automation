#pragma once

#if defined(AUTOMATION_LIBRARY)
#  define AUTOMATION_EXPORT Q_DECL_EXPORT
#else
#  define AUTOMATION_EXPORT Q_DECL_IMPORT
#endif

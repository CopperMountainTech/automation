#ifndef HISLIPREMOTECLIENT_H
#define HISLIPREMOTECLIENT_H

#include <QObject>
#include "RemoteClient.h"
#include <QQueue>
#include <functional>

namespace Planar {
namespace Automation {

const quint16 HislipPrologue = 18515;   //'H'*256 + 'S';    //"HS"
const quint16 HislipSvrVendor = 20556;  //'P'*256 + 'L';    //"PL"
const quint16 HislipAgString = 16711;   //'A'*256 + 'G'    //"AG"

const quint8 HislipInitialize = 0;
const quint8 HislipInitializeResponse = 1;
const quint8 HislipFatalError = 2;
const quint8 HislipError = 3;
const quint8 HislipAsyncLock = 4;
const quint8 HislipAsyncLockResponse = 5;
const quint8 HislipData = 6;
const quint8 HislipDataEnd = 7;
const quint8 HislipDeviceClearComplete = 8;
const quint8 HislipDeviceClearAcknowledge = 9;
const quint8 HislipAsyncRemoteLocalControl = 10;
const quint8 HislipAsyncRemoteLocalResponse = 11;
const quint8 HislipTrigger = 12;
const quint8 HislipInterrupted = 13;
const quint8 HislipAsyncInterrupted = 14;
const quint8 HislipAsyncMaximumMessageSize = 15;
const quint8 HislipAsyncMaximumMessageSizeResponse = 16;
const quint8 HislipAsyncInitialize = 17;
const quint8 HislipAsyncInitializeResponse = 18;
const quint8 HislipAsyncDeviceClear = 19;
const quint8 HislipAsyncServiceRequest = 20;
const quint8 HislipAsyncStatusQuery = 21;
const quint8 HislipAsyncStatusResponse = 22;
const quint8 HislipAsyncDeviceClearAcknowledge = 23;
const quint8 HislipAsyncLockInfo = 24;
const quint8 HislipAsyncLockInfoResponse = 25;

const quint8 HislipHeaderSize = 16;

//const int AsyncInputBufferSize = 4096;
const int SyncInputBufferSize = 32752;//65536 / 2 - 16;
const int OutputMessageSize = 1048576;

const quint8 UnidentifiedErrorCode = 0;
const quint8 HeaderErrorCode = 1;
const quint8 ConnectionsErrorCode = 2;

struct HislipMessage {
    quint16  Prologue;
    quint8    MessageType;
    quint8    ControlCode;
    quint32   Parameter;
    quint64  Length;
    QByteArray Data;            //???
};

typedef std::pair<QByteArray, quint32> MessagePair;



class HislipConverter
{
public:
    static void qByteArrayToMessage(QByteArray data, HislipMessage& message);
    static QByteArray messageToQByteArray(HislipMessage* message);
};

class HislipGlueParameters
{
public:
    HislipGlueParameters();
    void clearAll();

    void appendData(QByteArray data);
    int dataSize();
    QByteArray glueData();
    void cutDataFront(int count);

    int stage();
    void setStage(int value);

    void setExpectedSize(int size);
    int expectedSize();

private:
    QByteArray _glueData;
    int _glueStage;
    int _expectedDataSize;

};

//AUTOMATION_EXPORT нужно открыть, когда понадобится добавлять HislipMessageHandler из плагинов
class /*AUTOMATION_EXPORT*/ HislipRemoteClient : public RemoteClient
{
    Q_OBJECT
    typedef void(*HislipMessageHandler)(HislipMessage*, HislipRemoteClient*);
public:
    HislipRemoteClient(QTcpSocket* connection, quint16 sessionId,
                       quint32 parameter, QObject* parent = nullptr);
    ~HislipRemoteClient();

    void setAsyncConnection(QTcpSocket* asyncSocket);
    void sendFatalError(quint8 errorCode, QByteArray text);
    bool writeMessage(bool isSync, quint8 type, quint8 controlCode, quint32 parameter,
                      quint64 length, QByteArray data = QByteArray());

    bool isMessageIncomplete();
    void setMessageIncomplete(bool value);
    void appendDataToQueue(QByteArray data);
    void addCommandToQueue(QByteArray data, quint32 id);
    void removeNewlineInQueueBack();
    void setQueueBackParameter(quint32 parameter);
    void setMaxResponseSize(quint64 size);
    bool isAgileClient();
    void setFrmtExpected(bool value);
    void clearAll();
    bool isExclusiveLocked();
    void setExclusiveLock(bool value);
    static void registerSyncHandler(quint8 messageType,
                                    HislipMessageHandler handler);
    static void registerAsyncHandler(quint8 messageType, HislipMessageHandler handler);
    quint16 sessionId();

private:
    void syncDispatchMessage(HislipMessage* message);
    void asyncDispatchMessage(HislipMessage* message);
    void declareInterrupted();
    void asyncSendError(int errorCode, QByteArray text);
    void readMessage(QTcpSocket* connection, HislipGlueParameters* readParameters, bool isSync);

//    HislipMessage* makeMessage(quint8 messageType, quint8 controlCode,
//                              quint32 parameter, int dataSize, QByteArray data);

    bool writeSyncMessage(HislipMessage* message);

    bool writeAsyncMessage(HislipMessage* message);

    QTcpSocket* _asyncConnection;
    quint16 _sessionId;
    bool _parserBusy;
    bool _deviceClearTransaction;
    QQueue<MessagePair> _messageQueue;
    bool _frmtExpected;
    bool _agileClient;
    bool _messageIncomplete;
    bool _exclusiveLock;
    HislipGlueParameters* _asyncParameters;
    HislipGlueParameters* _syncParameters;
    quint64 _maximumResponseSize;

    static QMap<quint8, HislipMessageHandler> SyncMessageHandlers;
    static QMap<quint8, HislipMessageHandler> AsyncMessageHandlers;

private slots:
    virtual void onReadyRead();
    void onAsyncReadyRead();
    virtual void parseMessage();
};



}//Automation
}//Planar

#endif // HISLIPREMOTECLIENT_H
